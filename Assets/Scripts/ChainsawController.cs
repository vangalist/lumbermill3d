﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainsawController : MonoBehaviour
{
    public ParticleSystem sawdust;
    public Transform leftLimit;
    public Transform rightLimit;
    private Vector3 beginTouch;
    private Vector3 endTouch;
    private float dragCoefficent = 40f;
    public GameController gameController;
    public Rigidbody buzzsaw;
    public float rotationSpeed=3f;
    void Start(){
        buzzsaw.angularVelocity=Vector3.right*rotationSpeed;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Touch Started
            beginTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            //Touch is dragging
            endTouch = Input.mousePosition;
            Vector3 ofset = endTouch - beginTouch;
            dragChainsaw(convertScreenOfset2WorldOfset(ofset));
            beginTouch = endTouch;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            // Touch is ended
        }

    }
    void dragChainsaw(Vector3 ofset)
    {
        Vector3 newPosition = transform.position + dragCoefficent * ofset;
        newPosition.x = newPosition.x > rightLimit.position.x ? rightLimit.position.x : newPosition.x;
        newPosition.x = newPosition.x < leftLimit.position.x ? leftLimit.position.x : newPosition.x;
        transform.position=newPosition;
    }
    Vector3 convertScreenOfset2WorldOfset(Vector3 screenOfset)
    {
        // to normalize ofsets of touches ofset vector is divided to screen Width so it will work properly on wifferent screen sizes
        Vector3 worldOfset = (new Vector3(screenOfset.x, 0, 0)) / Screen.width;
        return worldOfset;
    }

    void OnTriggerEnter(Collider col){
        if(col.tag=="Lumber"){
            try
            {
                LumberController detectedLumber = col.GetComponent<LumberController>();
                detectedLumber.sliceLumber(this);
                sawdust.Play();
                gameController.aLumberSliced();
            }
            catch (System.Exception)
            {
                Debug.Log("Lumber Controller Could'nt found");
            }
        }
    }

    
}
