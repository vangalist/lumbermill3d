﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public StorageController storage;
    public Animator vibrateCameraAnimator;
    

    public int slicedLumberCounter = 0;
    public int storageOpeningPeriod = 5;

    public void aLumberSliced()
    {
        slicedLumberCounter += 1;
        if (slicedLumberCounter % storageOpeningPeriod == 0)
        {
            storage.openStorage();
        }
        vibrateCamera();
    }
    public void vibrateCamera(){
        vibrateCameraAnimator.Play("VibrateCamera",0);
    }
    public static void comboOccured(){
        Debug.Log("its a combo");
    }



}
