﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlicedLumberController : MonoBehaviour
{
    float lenght;
    float width;
    float ground=-20f;
    private void setSize(float lenght, float width)
    {
        transform.localScale = new Vector3(width, lenght, width);
    }
    private void setPosition(Vector3 newPosition){
        transform.position=newPosition;
    }
    private void setVelocity(Vector3 newVelocity){
        GetComponent<Rigidbody>().velocity=newVelocity;
    }
    public void initializeSlicedLumber(Vector3 newPosition,Vector3 newVelocity, float lenght, float width){
        setSize(lenght,width);
        setPosition(newPosition);
        setVelocity(newVelocity);
    }
    void Update(){
        if(transform.position.y<ground){
            Destroy(gameObject);
        }
    }
}
