﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumberGenerator : MonoBehaviour
{
    private float min_x =-9f;
    private float max_x = 9f;
    public GameObject LUMBER;
    public Transform lumberGenerationPosition;


    float lastLumberGenerationTime;
    public float lumberGenerationPeriod=1f;

    public void generateLumber(float cuttingAreaPosition, float x_cordinate,float Length){

        Vector3 newLumberCordinates = new Vector3(x_cordinate,lumberGenerationPosition.position.y,lumberGenerationPosition.position.z);
        GameObject newLumber = Instantiate(LUMBER,newLumberCordinates,lumberGenerationPosition.rotation);
        LumberController newLumberController = newLumber.GetComponent<LumberController>();
        newLumberController.setVolume(Length);

    }
    public void generateRandomLumber(){
        float LumberLenght=LumberController.getRandomLengt();
        float x_cordinate= Random.Range(min_x+LumberLenght,max_x-LumberLenght);
        float cuttingAreaPosition= Random.Range(-1,1);
        generateLumber(cuttingAreaPosition,x_cordinate,LumberLenght);
    }
    void Update(){
        float currentTime=Time.time;
        if(currentTime-lastLumberGenerationTime>lumberGenerationPeriod){
            generateRandomLumber();
            lastLumberGenerationTime=currentTime;
        }
    }


}
