﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumberController : MonoBehaviour
{
    //Lumbers has fixed Volume;
    public GameObject comboSlicingArea;
    public GameObject slicedLumber;
    Quaternion initialRotation;
    float initial_x_cordinate;
    static float maxLengt = 4f;
    static float minLenght = 2f;
    
    float lenght = 0;
    float volume = 2.8f * 2.8f * 2.8f;
    float width;

    void Start()
    {
        initialRotation = transform.rotation;
        initial_x_cordinate = transform.position.x;
    }

    void FixedUpdate()
    {
        transform.rotation = initialRotation;
        transform.position = new Vector3(initial_x_cordinate, transform.position.y, transform.position.z);
    }
    public static float getRandomLengt()
    {
        return Random.Range(minLenght, maxLengt);
    }
    public void setVolume(float newLenght)
    {
        lenght = newLenght;
        width = Mathf.Sqrt(volume / lenght);
        Vector3 newScale = new Vector3(width, lenght, width);
        transform.localScale = newScale;
        setComboSlicingArea(Random.Range(-1f,1f));
    }
    public void sliceLumber(ChainsawController chainsaw)
    {
        checkCombo(chainsaw);
        // Calculate Sliced Lumbers velocity
        Vector3 slicedLumberVelecity = GetComponent<Rigidbody>().velocity;
        slicedLumberVelecity.z /= 100;
        float sliceLumberWidth = width;
        // Calculate Sliced Lumbers Lenght
        float leftSlicedLumberLenght = chainsaw.transform.position.x - transform.position.x + lenght;
        float rightSlicedLumberLenght = 2 * lenght - leftSlicedLumberLenght;
        // Calculate Sliced Lumbers x Cordinate since other will be equal to former lumbers cordinates
        float leftSlicedLumberPosition_x = transform.position.x - lenght + leftSlicedLumberLenght / 2;
        float rightSlicedLumberPosition_x = transform.position.x + lenght - rightSlicedLumberLenght / 2;
        // Calculate Sliced Lumbers Position
        Vector3 leftSlicedLumberPosition = new Vector3(leftSlicedLumberPosition_x, transform.position.y, transform.position.z);
        Vector3 rightSlicedLumberPosition = new Vector3(rightSlicedLumberPosition_x, transform.position.y, transform.position.z);
        // Create Sliced Lumbers
        GameObject leftSlicedLumber = Instantiate(slicedLumber);
        GameObject rightSlicedLumber = Instantiate(slicedLumber);
        // Initialize Sliced Lumbers with calculated Values
        leftSlicedLumber.GetComponent<SlicedLumberController>().initializeSlicedLumber(
            leftSlicedLumberPosition, slicedLumberVelecity, leftSlicedLumberLenght / 2, width);
        rightSlicedLumber.GetComponent<SlicedLumberController>().initializeSlicedLumber(
            rightSlicedLumberPosition, slicedLumberVelecity, rightSlicedLumberLenght / 2, width);

        Destroy(gameObject);

    }

    private void setComboSlicingArea(float position){
        // position is between -1 and 1 its map to begining of Lumber to end of Lumber 
        // but i dont want it to creat Slicing area at limits so its remapped by dividing
        comboSlicingArea.transform.localPosition=new Vector3(0,position/1.3f,0);
    }
    private void checkCombo(ChainsawController chainsaw){
        // set errorRate 1.2 so when its close but not exact slicings is also counted as a combo
        float errorRate=1.2f;
        float comboSlicingAreaWidth= comboSlicingArea.transform.lossyScale.y;
        if(Mathf.Abs(chainsaw.transform.position.x-comboSlicingArea.transform.position.x)<comboSlicingAreaWidth*errorRate){
            GameController.comboOccured();
        }
        // checks if chainsaw hits green area of Lumber

    }


}
