﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageController : MonoBehaviour
{
     Animator animator;
    void Start(){
        animator=GetComponent<Animator>();
    }
    public void openStorage(){
        animator.Play("OpenDoors",0);
    }
}
